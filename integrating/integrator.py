class Integrator:
    """ Provides integrating functions. """

    @staticmethod
    def trapeze_integrate(f, x1, x2, n):
        """ Integrates functions with trapeze integration method.

        :param f: Callable function returning float value
        :param x1: Left bound of integration
        :param x2: Right bound of integration
        :param n: Number of regions which we would like to split our total area to
        :return: Result of integration
        """

        h = float(x2 - x1) / n
        result = 0.0
        for i in range(0, n):
            result += (f(x1 + i * h) + f(x1 + (i + 1) * h)) * h / 2
        return result


