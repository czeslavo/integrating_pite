from setuptools import setup


setup(
    name='integrating',
    version='0.1.0',
    author='Grzegorz Burzyński',
    author_email='czeslavo@gmail.com',
    description='Integrating app',
    license='BSD',
    packages=[
        'integrating'
    ],
    include_package_data=True,
    install_requires=[
        'pytest',
        'Click',
        'scipy',

    ],
    entry_points='''
        [console_scripts]
        integrating=integrating.app_manager:integrate
    '''
)
