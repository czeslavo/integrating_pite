import scipy.integrate
import timeit
import click
from integrating import Integrator


def compare_with_scipy(function, a, b, times, function_callable):
    """ Function to compare ours and SciPy's method of integrating in the time and accuracy domain.

    :param function: Function in form of a lambda expression in string
    :param a: Left bound of integrating
    :param b: Right bound of integrating
    :param times: How many times the methods should be called versus each other (more gives more accurate result)
    :param function_callable: Function object, which is passed to particular methods
    """
    my_setup = ("from integrating.integrator import Integrator\n"
                "func = " + function)
    scipy_setup = ("from scipy.integrate import quad\n"
                   "func = " + function)

    my_expr = "Integrator.trapeze_integrate(func, " + str(a) + ", " + str(b) + ", 100)"
    scipy_expr = "quad(func, " + str(a) + ", " + str(b) + ")"

    my_time = timeit.timeit(my_expr, my_setup, number=times)
    scipy_time = timeit.timeit(scipy_expr, scipy_setup, number=times)
    my_result = Integrator.trapeze_integrate(function_callable, a, b, 100)
    scipy_result = scipy.integrate.quad(function_callable, a, b)

    click.echo("Time of integrating.Integrator.trapeze_integrate(): " + str(my_time) + " sec.")
    click.echo("Time of scipy.integrate.quad() execution: " + str(scipy_time) + " sec.")
    click.echo("SciPy was " + str(my_time / scipy_time) + " times faster.")
    click.echo("integrating/SciPy result: " + str(my_result) + "/" + str(scipy_result[0]) +
               " (" + str(abs(my_result - scipy_result[0])) + " diff)")
