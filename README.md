## Integrating
CLI utility to integrate polynomial (as far as we are) functions.

## Usage example
Trapeze method:

    integrating trapeze "1.2 5 21" --bounds=0 12


gives us (1.2*x^2 + 5*x + 21)dx integral result calculated with trapeze method in bounds of [0, 12]

For more information check:

    integrating --help

## Dependencies
* click
* pytest
* scipy

## Installation
To install the utility with it's dependencies in your system, you should write in your terminal:

    cd project_root
    virtualenv venv && source venv/bin/activate
    python setup.py install


## Tests
    make test

