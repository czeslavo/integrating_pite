import pytest
from integrating.input_validator import InputValidator


def test_input_validator():
    valid_params = ["2.15", "-2.2"]
    not_valid_params = ["params", "wrong"]

    with pytest.raises(ValueError):
        InputValidator.valid_polynomial(not_valid_params)

    InputValidator.valid_polynomial(valid_params)
    assert valid_params == [2.15, -2.2]
