from .input_validator import InputValidator


class InputReader:
    """ Provides reading input (plain text file)
    Each file's row is the n-th parameter of a polynomial function .

    Uses InputValidator to check if the input is valid
    """

    def __init__(self):
        self.params = []

    def read_polynomial(self, function):
        """ Reads input data from user as polynomial function.
        Example:
        '2 3 -3.5' => 2x^2 + 3x - 3.5
        '2 0 1' => 2x^2 + 1

        :param function: String representing polynomial function
        """

        for word in reversed(function.split()):
            self.params.append(word)

        InputValidator.valid_polynomial(self.params)

    def produce_polynomial_function(self):
        """ Produces a polynomial function with read parameters."""

        def func(x):
            return sum([p * (x**i) for i, p in enumerate(self.params)])

        func.to_string = self._polynomial_to_string()
        func.to_lambda = self._polynomial_to_callable_lambda()
        return func

    def _polynomial_to_string(self):
        """ Produces a string representation of polynomial function.
        :return: String representation of polynomial function.
        """

        params_str = []
        for i, p in reversed(list(enumerate(self.params))):
            if i != 0:
                if p > 0:
                    params_str.append(str(p) + '*x^' + str(i))
                elif p < 0:
                    params_str.append('(' + str(p) + 'x^' + str(i) + ')')
            else:
                if p > 0:
                    params_str.append(str(p))
                elif p < 0:
                    params_str.append('(' + str(p) + ')')
        return ' + '.join(params_str)

    def _polynomial_to_callable_lambda(self):
        """
        :return: String of callable lambda expression of the polynomial function.
        """
        elements = []
        for i, p in reversed(list(enumerate(self.params))):
                    elements.append(str(p) + "*x**" + str(i))
        return "lambda x: " + ' + '.join(elements)
