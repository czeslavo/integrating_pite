from integrating.input_reader import InputReader
from integrating.integrator import Integrator
import os


def test_integrator_with_input():
    input_reader = InputReader()
    input_reader.read_polynomial('3 2')
    assert abs(Integrator.trapeze_integrate(
        input_reader.produce_polynomial_function(),
        0,
        5,
        100) - 47.5) < 0.01
