import click
from integrating import InputReader, Integrator
import integrating.compare


@click.group(help="Integrating by Grzegorz Burzyński.\n\n "
                  "For usage of particular methods, just type\n\n "
                  "\tintegrating method_name --help (i.e: integrating trapeze --help)")
def integrate():
    pass


@integrate.command(help="Integrates polynomial functions with a trapeze method.\n\n"
                        "Example:\n\n\t"
                        "integrating trapeze --bounds=0 1 --n=100 \"1 2 3\"  --  "
                        "will integrate 1.0*x^2 + 2*x + 3 dx slicing it onto 100 parts within [0,1] bounds")
@click.argument('parameters', type=str, metavar="POLYNOMIAL_PARAMETERS")
@click.option('--bounds', nargs=2, type=float, default=(0, 1), help="Bounds of integrating. Defaults are [0,1].")
@click.option('--n', type=int, default=100, help="Number of slices which the function'll be split on. Default is 100.")
@click.option('--compare', type=bool, is_flag=True, help="Compare with SciPy integrating. It's a flag.")
def trapeze(parameters, bounds, n, compare):
    input_reader = InputReader()
    try:
        input_reader.read_polynomial(parameters)
    except ValueError as verr:
        click.echo(verr)
        exit()

    function = input_reader.produce_polynomial_function()

    result = Integrator.trapeze_integrate(
        function,
        bounds[0],
        bounds[1],
        n
    )
    click.echo(click.style('RESULT:', bold=True, bg='green'))
    click.echo(click.style(u"\u222B" + " [" + str(bounds[0]) + ", " + str(bounds[1]) + "]  " +
                           function.to_string + " = " + str(result), bg='green'
                           )
               )

    if compare:
        click.echo(click.style('COMPARISON WITH SCIPY:', bold=True, bg='yellow', fg='black'))
        integrating.compare.compare_with_scipy(function.to_lambda,
                                               bounds[0],
                                               bounds[1],
                                               100,
                                               function)
