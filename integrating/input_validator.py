class InputValidator:
    """ Validates an user's input. """

    @staticmethod
    def valid_polynomial(params):
        """ Validates a list of parameters of polynomial function.
        Valid input is a list of strings, which can be casted to int.
        Otherwise it raises ValueError.

        :param params: List of parameters - strings
        """

        for i, param in enumerate(params):
            try:
                params[i] = float(param)
            except ValueError:
                raise
