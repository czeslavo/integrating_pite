from integrating import InputReader
import os


def test_input_reader_polynomial_function():
    input_reader = InputReader()
    input_reader.read_polynomial('3 2')
    func = input_reader.produce_polynomial_function()
    assert func(2) == 8


def test_input_reader_params():
    params = [2.0, 3.0]
    input_reader = InputReader()
    input_reader.read_polynomial('3 2')
    assert params == input_reader.params


def test_lambda():
    lambda_exp = 'lambda x: 3.0*x**1 + 2.0*x**0'
    input_reader = InputReader()
    input_reader.read_polynomial('3 2')
    function = input_reader.produce_polynomial_function()
    print(function.to_lambda)
    assert lambda_exp == function.to_lambda
