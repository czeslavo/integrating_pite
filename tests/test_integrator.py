from integrating.integrator import Integrator


def test_integrator():
    result = Integrator.trapeze_integrate(lambda x: x**2, 0, 5, 1000)
    assert abs(result - 41.667) < 0.5

    result = Integrator.trapeze_integrate(lambda x: x**4 + x**2 + 4 * x, 0, 15, 1000)
    assert abs(result - 153450) < 0.5

